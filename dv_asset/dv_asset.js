#!/usr/bin/env node

var path = require("path");
var nopt = require("nopt");
var os = require("os");
var fs = require("fs-extra");
var express = require("express");
var https = require('https');
var http = require('http');
var RED = require('node-red');
var util = require("util");
var request = require('request-promise');
var url = require("url");

try { var admin_comm_list = require("./admin_comm.js"); }
catch (err) { var admin_comm_list = ["http:localhost:1888","https:localhost:1888"]; }


process.env.NODE_RED_HOME = process.env.NODE_RED_HOME || path.join(__dirname, "node_modules", "node-red");
process.env['CERT_PATH'] = os.homedir() + '/dv/.certs/node_certs/dv_asset/dv_asset.pem';
process.env['KEY_PATH'] = os.homedir() + '/dv/.certs/node_certs/dv_asset/dv_asset.key';
//process.env['NODE_EXTRA_CA_CERTS'] = os.homedir() + '/dv/.certs/CA/rootCA.pem';

console.log("DV Asset start up!!")
//Check if System homedir is defined
if (os.homedir() === null) {
    //If not defined, DV cant be instantiated
    console.log("Unsupported System or Platform error - Undefined home directory");
    console.log("Exiting application...");
    process.exit(1);
}


//Directories
var mainDir = path.join(os.homedir(), "nova_dv", "dv_asset");
var runtimeDir = path.join(os.homedir(), ".nova_dv", ".dv_asset");
var flowFile_path = path.join(runtimeDir, "dv_asset_flows.json");
var redSettingsFile = path.join(mainDir, "dv_asset_nodered_settings.js");
var specificDir = path.join(os.homedir(), ".nova_dv",".dv_specific");
var assetSettingsFile = path.join(mainDir, "dv_asset_config.json");

var server;
var app = express();


//Arguments parser
var knownOpts = {
    "help": Boolean,
    "port": Number,
    "verbose": Boolean,
    "safe": Boolean
};
var shortHands = {
    "h": ["--help"],
    "p": ["--port"],
    "v": ["--verbose"]
};
nopt.invalidHandler = function (k, v, t) {
    // TODO: console.log(k,v,t);
}
var parsedArgs = nopt(knownOpts, shortHands, process.argv, 2)
if (parsedArgs.help) {
    console.log("Usage: DVasset [-v] [--port PORT][--safe]");
    console.log("");
    console.log("Options:");
    console.log("  -p, --port     PORT  port to listen on");
    console.log("  -v, --verbose        enable verbose output");
    console.log("      --safe           enable safe mode");
    console.log("  -h, --help           show this help");
    console.log("");
    process.exit();
}



//CHECK DIRECTORIES...
/*
Check is runtimeDir exists
If not, it tries to create the dir: HOME/.defender/.dv/asset
TODO: Close app in order to the asset be able to run instalation script
*/
if (!fs.pathExistsSync(runtimeDir)) {
    fs.mkdirsSync(runtimeDir);
    console.log("Created runtimeDir: " + runtimeDir);
    console.log("Instalation Script should be run before launching app..");
}
if (!fs.pathExistsSync(specificDir)) {
    fs.mkdirsSync(specificDir);
    console.log("Created specificDir: " + specificDir);
}
/*
Check settings file
Instaltion Script should install all
If not present, it can end process or copy a default settings
*/
if (!fs.existsSync(redSettingsFile)) {
    if (fs.existsSync(path.join(__dirname, "dv_asset_nodered_settings.js"))) {
        fs.copyFileSync(path.join(__dirname, "dv_asset_nodered_settings.js"), redSettingsFile);
        console.log("Load default settings for node-red");
    } else {
        console.log("No settings file for launching node-red instance");
        console.log("Instalation Script should be run before launching app..");
        process.exit(3);
    }
}



/*////////////////
Script section for preparing settings for node-red
Based on node-red module, but adopted for DV case (mandatory HTTPS, port, etc.)
*/

try {
    //load settings to from file into cache
    var settings = require(redSettingsFile);
    settings.settingsFile = redSettingsFile;
} catch (err) {
    console.log("Error loading settings file: " + redSettingsFile)
    if (err.code == 'MODULE_NOT_FOUND') {
        if (err.toString().indexOf(redSettingsFile) === -1) {
            console.log(err.toString());
        }
    } else {
        console.log(err);
    }
    process.exit();
}

settings.functionGlobalContext={
    args: process.env,
    path:path,
    fs:fs,
    os:os,
    request:request,
    url:url
    //node_args:process.env.node_args,
    //admin_comm:process.env.ADMIN_COMM,
}


settings.contextStorage={
    default: "memoryOnly",
    memoryOnly: { module: 'memory' },
    file: { 
        module: 'localfilesystem',
        config:{
            dir:runtimeDir,
            cache:false
        }
    }
}
settings.functionGlobalContext.admin_comm_list=admin_comm_list;
// Verbose and safe mode to be checked
if (parsedArgs.verbose) {
    settings.verbose = true;
}
if (parsedArgs.safe) {
    settings.safeMode = true;
}

//Look for port specified by user
if (parsedArgs.port) {
    console.log("Custom port has been declared: " + parsedArgs.port);
    settings.uiPort = parsedArgs.port;
} else {
    console.log("Using default port: 1889");
    //settings.uiPort=1889; 
}

//This will set all IPv4 interfaces to listen, or a given location if defined in red_settings file
settings.uiHost = settings.uiHost || "127.0.0.1";

if(settings.uiHost=="0.0.0.0"){
    process.env.MYIP="127.0.0.1";
}else{
    process.env.MYIP=settings.uiHost;
}


///////////////////////////////////
//TODO: CHECK THIS
settings.httpRoot = false;
if (settings.httpAdminRoot !== false) {
    settings.httpAdminRoot = formatRoot(settings.httpAdminRoot || "/");
    settings.httpAdminAuth = settings.httpAdminAuth || settings.httpAuth;
} else {
    settings.disableEditor = true;
}
if (settings.httpNodeRoot !== false) {
    settings.httpNodeRoot = formatRoot(settings.httpNodeRoot || "/");
    settings.httpNodeAuth = settings.httpNodeAuth || settings.httpAuth;
} else {
    settings.httpNodeRoot = formatRoot("defender");
}
settings.flowFile = settings.flowFile || flowFile_path;
settings.userDir = settings.userDir || runtimeDir;



let server_prot="http:";
///////////////////////
//Try HTTPS, and then HTTP
if (fs.pathExistsSync(process.env.CERT_PATH) && fs.pathExistsSync(process.env.KEY_PATH) && process.env.HTTPS_ON=="true") {
    try {
        settings.https = {
            key: fs.readFileSync(process.env.KEY_PATH),
            //ca: fs.readFileSync(require('os').homedir() + '/dv/.certs/CA/rootCA.pem'), 
            cert: fs.readFileSync(process.env.CERT_PATH),
            requestCert: false, //This must be settled to true for secure app
            rejectUnauthorized: false //This must be settled to true for secure app
        };
        settings.requireHttps = true;
        server = https.createServer(settings.https, app);
        console.log("Created HTTPS server");
        server_prot="https:";
    } catch (err) {
        settings.requireHttps = false;
        settings.https = false;
        console.log("unable to start with secure HTTPS");
        try {
            server = http.createServer(app);
            console.log("Created HTTP server");
        } catch (err) {
            console.log("unable to start server");
            process.exit(1);
        }
    }
} else {
    try {
        server = http.createServer(app);
        console.log("Created HTTP server");
    } catch (err) {
        console.log("unable to start server");
        process.exit(1);
    }
}
server.setMaxListeners(0);

settings.functionGlobalContext.flows = RED.runtime.flows;
//console.log(settings);
try{
    settings.functionGlobalContext.specific=require(assetSettingsFile);
}catch(err){
    settings.functionGlobalContext.specific=[];
}

init();

settings.functionGlobalContext.device={
    host:process.env.MYIP,
    port:settings.uiPort,
    type:"asset",
    prot:server_prot,
    url:server_prot+"//"+process.env.MYIP+":"+settings.uiPort
};
 

async function init() {

    //Check for an available A&M device
    for (var k in admin_comm_list) {
        var options_admin = { timeout: 500 };
        var prot = url.parse(admin_comm_list[k]).protocol;
        if (prot === "https:") {
            try{
                options_admin.key = fs.readFileSync(process.env.KEY_PATH);
                options_admin.cert = fs.readFileSync(process.env.CERT_PATH);
            }catch(err){
                options_admin.key=undefined;
                options_admin.cert=undefined;
            }
        }
        options_admin.url = admin_comm_list[k] + "/dv_admin/ping";
        try {
            var r = await request.get(options_admin);
            console.log(r)
            settings.functionGlobalContext.current_admin=admin_comm_list[k];
            console.log("A&M available at: "+admin_comm_list[k]);
            break;
        } catch (err) {
            //console.log(err);
            console.log("Unbale to ping A&M at: "+admin_comm_list[k]);
        }
    }

    //When starting up,if a flows file exists, get id of the main flow, and set all other to disable
    if(fs.existsSync(settings.flowFile)){
        try{
            let ff=JSON.parse(fs.readFileSync(settings.flowFile));

            if (ff[0].type==='tab' && ff[0].label==="DV_ASSET"){
                //Store main flow id on global
                //TODO: store in context file and activate storage from localfile
                settings.functionGlobalContext.mainId=ff[0].id;
                for (var i in ff){
                    if(i!=='0'){
                        if(ff[i].type==="tab"){
                            ff[i].disabled=true;
                        }else{
                            fs.writeFileSync(settings.flowFile,JSON.stringify(ff));
                            break;
                        }
                    }
                }
                //console.log(ff);
            }else{
                throw new Error("Bad formed flow file: Expected DV_Asset flow at position 0");
            }
            
        }catch(err){
            //TODO: replace flows file with default flow file 
            console.log(err);
        }
    }
    try {
        RED.init(server, settings);
    } catch (err) {
        if (err.code == "unsupported_version") {
            console.log("Unsupported version of node.js:", process.version);
            console.log("Node-RED requires node.js v4 or later");
        } else if (err.code == "not_built") {
            console.log("Node-RED has not been built. See README.md for details");
        } else {
            console.log("Failed to start server:");
            if (err.stack) {
                console.log(err.stack);
            } else {
                console.log(err);
            }
        }
        process.exit(1);
    }

    if (settings.httpAdminRoot !== false) {
        app.use(settings.httpAdminRoot, RED.httpAdmin);
    }
    if (settings.httpNodeRoot !== false) {
        app.use(settings.httpNodeRoot, RED.httpNode);
    }

    /* ///// WHAT IS STATIC IN HERE????
        if (settings.httpStatic) {
        settings.httpStaticAuth = settings.httpStaticAuth || settings.httpAuth;
        if (settings.httpStaticAuth) {
            app.use("/",basicAuthMiddleware(settings.httpStaticAuth.user,settings.httpStaticAuth.pass));
        }
        app.use("/",express.static(settings.httpStatic));
    }
    */

    RED.start().then(function () {
        console.log("Starting Node_red");
        if (settings.httpAdminRoot !== false || settings.httpNodeRoot !== false || settings.httpStatic) {
            server.on('error', function (err) {
                if (err.errno === "EADDRINUSE") {
                    RED.log.error(RED.log._("server.unable-to-listen", { listenpath: getListenPath() }));
                    RED.log.error(RED.log._("server.port-in-use"));
                } else {
                    RED.log.error(RED.log._("server.uncaught-exception"));
                    if (err.stack) {
                        RED.log.error(err.stack);
                    } else {
                        RED.log.error(err);
                    }
                }
                process.exit(1);
            });
            server.listen(settings.uiPort, settings.uiHost, function () {
                if (settings.httpAdminRoot === false) {
                    RED.log.info(RED.log._("server.admin-ui-disabled"));
                }
                settings.serverPort = server.address().port;
                process.title = parsedArgs.title || 'DV_asset';
                RED.log.info(RED.log._("server.now-running", { listenpath: getListenPath() }));
                //process.env.device=JSON.stringify(server.address());
            });
        } else {
            RED.log.info(RED.log._("server.headless-mode"));
        }/*}).then(function(){ */
    }).otherwise(function (err) {
        RED.log.error(RED.log._("server.failed-to-start"));
        if (err.stack) {
            RED.log.error(err.stack);
        } else {
            RED.log.error(err);
        }
    });
    console.log("Ending DV launch script");
}



////////////////AUX FUNCTIONS
function getListenPath() {
    var port = settings.serverPort;
    if (port === undefined) {
        port = settings.uiPort;
    }

    var listenPath = 'http' + (settings.https ? 's' : '') + '://' +
        (settings.uiHost == '::' ? 'localhost' : (settings.uiHost == '0.0.0.0' ? '127.0.0.1' : settings.uiHost)) +
        ':' + port;
    if (settings.httpAdminRoot !== false) {
        listenPath += settings.httpAdminRoot;
    } else if (settings.httpStatic) {
        listenPath += "/";
    }
    return listenPath;
}


//formating https routes: adding "/" before and after
function formatRoot(_root) {
    if (_root[0] != "/") {
        _root = "/" + _root;
    }
    if (_root.slice(-1) != "/") {
        _root = _root + "/";
    }
    return _root;
}

process.on('uncaughtException', function (err) {
    util.log('[DV] Uncaught Exception:');
    if (err.stack) {
        util.log(err.stack);
    } else {
        util.log(err);
    }
    process.exit(1);
});

process.on('SIGINT', function () {
    RED.stop().then(function () {
        process.exit();
    });
});