# Double Virtualization (DV)
Software module for networked solutions for automatic management of assets and processes.



## DV Asset
This repository contains the necessary files for installing and running the **DVAsset** module of the Double Virtualization.

By installing and running these on a Asset, it will become adopted with the necessary logic to be part of a DV system.

---
## Requirements
- **node.js** (v10 and v12 tested)

The DV application runs as a **node.js** application.

The solution as been tested on *Windows*, *MacOS* and *Unix* systems.

---
## Install

To install the DVAsset:

1. Browse into the repository directory
2. execute the install script, as **node.js application**
	- *$node install_dv_asset.js*

The script creates one directory for the application files and one for the runtime files, both relative to the current account HOME directory:

- *$~/nova_dv/dv_asset* - application directory  
- *$~/.nova_dv/dv_asset* - runtime directory

---
## Setup and run

Browse into the application directory

### Setup files

Edit the file with the runtime configuration

- **dv_asset_settings.js**
	- host(s): *default:* 0.0.0.0 (all available IPV4)
	- port *default:* 1889
	- HTTPS (include certificate paths)
	- ...

- **admin_comm.js**
	- Configure possible *DV&AM* hosts 

- **dv_asset_config.js**
	- automatic load of applications on startup 

### execution

execute the node app **dv_asset.js**

- *$node dv_asset.js*

---
## Authors
- Guilherme Brito
- Giovanni di Orio
- Pedro Mal�

Uninova / Nova School of Science (Lisbon)
